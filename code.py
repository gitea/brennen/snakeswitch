"""
Main program loop for SnakeSwitch

https://github.com/adafruit/snakeswitch

* Author(s): Brennen Bearnes
"""
import board
from snakeswitch import SnakeSwitch
from switch_config import LAYOUTS

SWITCH_PINS = (board.D5, board.D6, board.D9)

ss = SnakeSwitch(SWITCH_PINS, LAYOUTS)
while True:
    ss.advance_frame()
