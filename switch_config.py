"""
SnakeSwitch Configuration
=========================

This file should define a dictionary called LAYOUTS.

The keys of this dictionary should be integers corresponding to
NeoTrelllis button numbers, 0-15.  Values may be either a
dictionary defining a layout, or a tuple containing keycodes
to fire immediately.
"""

from adafruit_hid.keycode import Keycode

# Define a modifier key here for easy changing if window manager
# configuration changes - this is the Windows key on most keyboards:
MOD_KEY = Keycode.LEFT_GUI

LAYOUTS = {

    # A default layout - just the modifier key on the 0th switch:
    0: {
        0: MOD_KEY,
    },

    # The second button toggles left and right arrows on the other two
    # switches:
    1: {
        1: Keycode.LEFT_ARROW,
        2: Keycode.RIGHT_ARROW,
    },

    # The third button toggles chorded mod-left, mod-right - workspace
    # switching in my XMonad setup:
    2: {
        1: (MOD_KEY, Keycode.LEFT_ARROW),
        2: (MOD_KEY, Keycode.RIGHT_ARROW),
    },

    3: {
        1: Keycode.PAGE_UP,
        2: Keycode.PAGE_DOWN,
    },

    # These add some common chords to the primary mod key if used
    # in combination with layout 0:

    # Mod-Shift-G - brings up a list of active windows:
    4: {
        0: (Keycode.SHIFT, Keycode.G)
    },

    # Add this for mod-tab:
    5: {
        0: Keycode.TAB
    },

    # Instead of toggling layouts for the footswitches, pressing and
    # releasing buttons 8-15 will instantly fire keyboard events, in
    # this case switching between workspaces:
    15: (MOD_KEY, Keycode.ONE),
    14: (MOD_KEY, Keycode.TWO),
    13: (MOD_KEY, Keycode.THREE),
    12: (MOD_KEY, Keycode.FOUR),
    11: (MOD_KEY, Keycode.FIVE),
    10: (MOD_KEY, Keycode.SIX),
    9:  (MOD_KEY, Keycode.SEVEN),

    # Toggle note window:
    8: (MOD_KEY, Keycode.SHIFT, Keycode.N)

}
